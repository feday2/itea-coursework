class Post{

    static _id = 0;

    constructor(author, text, imageUrl = null, image = null, likes = 0, comments = []) {
        this.id = Post._id;
        Post._id = Post._id + 1;
        this.author = author;
        this.text = text;
        this.imageUrl = imageUrl;
        this.image = image;
        this.likes = likes;
        this.comments = comments;
        this.publishDate = new Date();
        this.commentShow = 0;
    }

    addLike(){
        this.likes = this.likes + 1;
    }

    addComment(comment){
        this.comments.push(comment);
    }

    renderComments(rootEl){
        rootEl.innerText = '';
        this.comments.forEach( function(comment) {
            let commentEl = `
            <div>
                <b>${comment.author}</b></br>
                ${comment.text}
            </div>
            `;
            
            rootEl.innerHTML = rootEl.innerHTML + commentEl;
        })
    }
}

class PostCollection{
    constructor(posts = []) {
        this.posts = posts;
    }

    addPost(post){
        this.posts.push(post);
    }

    render(rootEl){
        rootEl.innerHTML = '';
        let collection = this;
        this.posts.forEach( function(post){
            collection.renderOne(rootEl, post);
        })
    }

    renderOne(rootEl, post) {
        let postImage = "";
        if (post.image) {
            postImage = post.image;
        } else if(post.imageUrl) {
            postImage = post.imageUrl;
        }
        let postHtmlText = `<div class="post">
                <div>
                     Author <b>${post.author}</b>
                </div>
                <div>
                     Publish Date <b>${post.publishDate.toLocaleString()}</b>
                </div>
                <div>
                    <img src="${postImage}">
                </div>
                <div>
                    ${post.text}
                </div>
                <div>
                    <button class="addLike">add like</button>
                    likes = <span class="likesCount">${post.likes}<span>
                </div>
                <div>
                    <button class="toggleComments">Comments (0)</button>
                    <div class="comments"></div>
                    <form>
                        <input type="text" name="commentAuthor" placeholder="author" required>
                        <input type="text" name="commentText" placeholder="comment" required>
                        <button>add comment</button>
                    </form>
                </div>
            </div>`
        let postEl = document.createRange().createContextualFragment(postHtmlText);
        postEl.querySelector(".addLike").addEventListener("click", function(e) {
            post.addLike();
            e.target.parentElement.querySelector(".likesCount").innerText = post.likes;
        });
        postEl.querySelector("form").addEventListener("submit", function(e) {
            e.preventDefault();
            let comment = {
                author: e.target.commentAuthor.value,
                text: e.target.commentText.value
            };
            e.target.commentAuthor.value = "";
            e.target.commentText.value = "";
            post.addComment(comment);
            e.target.parentElement.querySelector(".toggleComments").innerText = `Comments (${post.comments.length})`;
            if (post.commentShow === 1) {
                post.renderComments(e.target.parentElement.querySelector(".comments"));
            }
        });
        postEl.querySelector(".toggleComments").addEventListener("click", function(e) {
            if (post.commentShow === 0) {
                post.renderComments(e.target.parentElement.querySelector(".comments"));
                post.commentShow = 1;
            } else {
                e.target.parentElement.querySelector(".comments").innerHTML = "";
                post.commentShow = 0;
            }
            
        });
        rootEl.appendChild(postEl);
    }
}

const myForm = document.getElementById("myForm");
const postsRootEl = document.getElementById("posts");
const imgPreviewEl = document.getElementById("imgPreview");
var collection = new PostCollection();
var loadedImg = null;

myForm.image.addEventListener("change", (e) => {
    e.preventDefault();
    var fileReared = new FileReader();
    fileReared.addEventListener("load", function(e) {
        loadedImg = e.target.result;
        imgPreviewEl.innerHTML = `<img src="${loadedImg}"></img>`
    }); 
    fileReared.readAsDataURL( e.target.files[0] );
})

myForm.addEventListener("submit", (e) => {
    e.preventDefault();
    let author = myForm.author.value;
    let text = myForm.postText.value;
    let imageUrl = myForm.imageUrl.value;
    let img = loadedImg;

    myForm.author.value = "";
    myForm.postText.value = "";
    myForm.imageUrl.value = "";
    loadedImg = null;
    imgPreviewEl.innerHTML = "";

    let post = new Post(author, text, imageUrl, img);
    collection.addPost(post);
    collection.renderOne(postsRootEl, post);
});